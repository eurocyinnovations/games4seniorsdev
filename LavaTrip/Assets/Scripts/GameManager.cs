﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {

    public static string gameMode;
    public GameObject menuPanel;
    public GameObject gamePanel;
    public GameObject homePanel;
    public GameObject levelPanel;
    public GameObject homeMenu;
    public Text Starttxt;

    public Button startButton;

    public Button pauseButton;
    public GameObject spawner;
    public Button playButton;
    public static bool pause;

    public Button continueButton;
    public Button exitButton;
    public Button exitButton2;
    public Button levelButton;
    public Button homeMenuButton;
    public Button restartButton;
    public Button exitButton3;

    public GameObject losePanel;

    // Start is called before the first frame update
    void Start () {

        Scene currentScene = SceneManager.GetActiveScene ();
        string sceneName = currentScene.name;

        switch (sceneName) {
            case "MenuScene":
                playButton.onClick.AddListener (() => StartCoroutine (playButtonListener ()));
                levelButton.onClick.AddListener (() => levelButtonListener ());
                homeMenuButton.onClick.AddListener (() => homeMenuButtonListener ());

                break;
            case "GameScene":
                stateGame ("PlayGame");
                setPanel ("PlayGame");
                pauseButton.onClick.AddListener (() => pauseButtonListener ());
                continueButton.onClick.AddListener (() => continueButtonListener ());
                exitButton.onClick.AddListener (() => exitButtonListener ());
                exitButton2.onClick.AddListener (() => exitButtonListener ());
                exitButton3.onClick.AddListener (() => exitButtonListener ());
                restartButton.onClick.AddListener (() => restartGameButton ());

                break;
            default:
                break;
        }
    }

    #region Listeners
    IEnumerator playButtonListener () {
        yield return new WaitForSeconds (1.20f);
        pause = false;
        Spawner.kindofGame = "NormalGame";
        SceneManager.LoadScene ("GameScene");

    }

    private void pauseButtonListener () {
        pause = true;
        setPanel ("PauseGame");

    }

    private void continueButtonListener () {
        pause = false;
        setPanel ("PlayGame");

    }

    private void exitButtonListener () {
        pause = true;
        SoundManager.isMuted = false;
        SceneManager.LoadScene ("MenuScene");
    }
    private void levelButtonListener () {
        homeMenu.SetActive (false);
        homePanel.SetActive (false);
        levelPanel.SetActive (true);
    }
    private void homeMenuButtonListener () {
        homeMenu.SetActive (true);
        homePanel.SetActive (true);
        levelPanel.SetActive (false);
    }

    #endregion

    void stateGame (string gameType) {
        switch (gameType) {
            case "PlayGame":
                break;
            case "StartGameAgain":
                startButton.interactable = pause;
                break;
                //      case "MenuGame": DestroyCubes(); start = false; break;
            default:
                break;
        }

        Spawner.startCoroutineOneTime = pause;

        spawner.GetComponent<Spawner> ().enabled = !pause;

    }

    private void setPanel (string gameMode)

    {
        switch (gameMode) {
            case "PlayGame":

                gamePanel.SetActive (true);
                menuPanel.SetActive (false);
                break;
            default:
                gamePanel.SetActive (false);
                menuPanel.SetActive (true);
                break;
        }
    }

    public void ChangeScene (int level) {

        Spawner.kindofGame = "levelGame";

        switch (level) {
            case 1:
                Spawner.spawnWait = 2.5f;
                Movement.movementSpeed = 1.5f;
                break;
            case 2:
                Spawner.spawnWait = 2.5f;
                Movement.movementSpeed = 2.5f;
                break;
            case 3:
                Spawner.spawnWait = 2.0f;
                Movement.movementSpeed = 3.2f;
                break;
            case 4:
                Spawner.spawnWait = 2.0f;
                Movement.movementSpeed = 3.7f;
                break;
            case 5:
                Spawner.spawnWait = 1.8f;
                Movement.movementSpeed = 3.6f;
                break;
            case 6:
                Spawner.spawnWait = 1.5f;
                Movement.movementSpeed = 4.3f;
                break;
            case 7:
                Spawner.spawnWait = 1.5f;
                Movement.movementSpeed = 4.8f;
                break;
            case 8:
                Spawner.spawnWait = 1.3f;
                Movement.movementSpeed = 4.0f;
                break;
            case 9:
                Spawner.spawnWait = 1.0f;
                Movement.movementSpeed = 4.5f;
                break;

            default:

                break;

        }

        startPlay ();

    }

    public void restartGameButton () {
        losePanel.SetActive (false);
        gamePanel.SetActive (true);
        ScoringManager.score = 0;
        GameManager.pause = false;
        Spawner.startCoroutineOneTime = false;
        GameObject[] redCubes = GameObject.FindGameObjectsWithTag ("redCube");
        GameObject[] blueCubes = GameObject.FindGameObjectsWithTag ("blueCube");
        foreach (GameObject redCube in redCubes)
            GameObject.Destroy (redCube);
        foreach (GameObject blueCube in blueCubes)
            GameObject.Destroy (blueCube);
        Spawner.time = 0;
        TimeScript.currentTime = 0;

    }

    public void startPlay () {
        pause = false;
        setPanel ("PlayGame");
        SceneManager.LoadScene ("GameScene");

    }

}