﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

    public AudioSource audioSource;
    public AudioClip losePoint;
    public AudioClip winPoint;

    public static bool isMuted;
    public static AudioSource extraAudioSource;



    // Update is called once per frame

    private void Start()
    {
        extraAudioSource = gameObject.AddComponent<AudioSource>();
       
    }

    public void audioSourceState()
    {
        if (isMuted == true)
        {
            audioSource.mute = false;
            isMuted = false;
            return;
        }
        audioSource.mute = true;
        isMuted = true;
    }



    public void losePointSound()
    {
        if (!isMuted) extraAudioSource.PlayOneShot(losePoint);
    }

    public void winPointSound()
    {
        if (!isMuted) extraAudioSource.PlayOneShot(winPoint);
    }
}
