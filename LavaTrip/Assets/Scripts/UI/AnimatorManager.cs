﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AnimatorManager : MonoBehaviour {

    public Animator animator;
    bool waitbefore = false;
    int count;
    public Button playButton;

    void Start () {

        Application.targetFrameRate = 60;
        playButton.onClick.AddListener (() => playButtonListener ());
    }

    private void playButtonListener () {
        StartCoroutine (ExampleCoroutine ());
    }

    IEnumerator ExampleCoroutine () {
        animator.SetBool ("clicked", true);
        yield return new WaitForSeconds (1);
    }

}