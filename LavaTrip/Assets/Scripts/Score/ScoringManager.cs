﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringManager : MonoBehaviour {

    public static int score = 0;
    public Text blockScoreLabel;
    public AudioSource audioSource;
    public GameObject losePanel;
    public GameObject gamePanel;

    private static int oldHighScore;

    private void Start () {
        score = 0;
        oldHighScore = +PlayerPrefs.GetInt ("Highscore");
    }
    private void OnCollisionEnter (Collision other) {

        if (gameObject.name == other.gameObject.tag) {
            score += 1;
            audioSource.GetComponent<SoundManager> ().winPointSound ();
            if (oldHighScore < score && Spawner.kindofGame != "levelGame") {
                PlayerPrefs.SetInt ("Highscore", score);
            }
        } else {
            score -= 1;
            audioSource.GetComponent<SoundManager> ().losePointSound ();
        }
        Destroy (other.gameObject);

        blockScoreLabel.text = "Blocks " + score;
        if (Spawner.kindofGame != "levelGame")
            if (score == -1) gameOver ();
    }

    void gameOver () {
        losePanel.SetActive (true);
        gamePanel.SetActive (false);
        score = 0;
        blockScoreLabel.text = "Blocks " + score;
        GameManager.pause = true;
    }

}