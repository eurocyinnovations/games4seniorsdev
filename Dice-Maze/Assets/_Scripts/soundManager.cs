﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class soundManager : MonoBehaviour
{   
    public AudioSource playMusic;
    static AudioSource getcoin;
    static AudioSource getime;
    static AudioSource hitwall;

    public static bool mute = false;

    private AudioSource[] audioSourcesComponents;


    void Start()
    {

        audioSourcesComponents = GetComponents<AudioSource>();
        getcoin = audioSourcesComponents[0];
        getime = audioSourcesComponents[1];
        hitwall = audioSourcesComponents[2];
    }

    // Update is called once per frame
    public static void coinCollisionSound()
    {
        if (mute == false) getcoin.Play();
    }

    public static void timeCollisionSound()
    {
        if (mute == false) getime.Play();
    }

    public static void wallCollisionSound()
    {
        if (mute == false) hitwall.Play();
    }

    public void muteHandler()
    {   
         mute = !mute;
        if (!mute)playMusic.Pause();
        if (mute)playMusic.Play();
       
    }

}
